package spinner

import (
	"context"
	"io"
	"os"
	"os/signal"
	"runtime"
	"sync"
	"time"

	"github.com/mattn/go-colorable"
)

// SpinnerManager manages spinners
type SpinnerManager interface {
	AddSpinner(msg string) *Spinner
	GetSpinners() []*Spinner
	SetSpinnersCount(count int)
	Start()
	Stop()
}

type spinnerManager struct {
	spinners []*Spinner
	mutex    sync.RWMutex

	writer  io.Writer
	context context.Context

	done      chan bool
	hasUpdate chan bool
	ticks     *time.Ticker
	frame     int
}

// constructor for the SpinnerManager.
func NewSpinnerManager(ctx context.Context) SpinnerManager {
	sm := &spinnerManager{
		writer:    getWriter(),
		context:   ctx,
		done:      make(chan bool),
		hasUpdate: make(chan bool),
	}
	return sm
}

// AddSpinner adds a new spinner to the manager.
func (sm *spinnerManager) AddSpinner(message string) *Spinner {
	spinner := sm.createSpinner(message)

	sm.mutex.Lock()
	defer sm.mutex.Unlock()
	sm.spinners = append(sm.spinners, spinner)

	return spinner
}

// Create a new spinner with manager options.
func (sm *spinnerManager) createSpinner(message string) *Spinner {
	opts := SpinnerOptions{
		Message:   message,
		HasUpdate: sm.hasUpdate,
	}
	return NewSpinner(opts)
}

// GetSpinners returns the spinners managed by the manager.
func (sm *spinnerManager) GetSpinners() []*Spinner {
	sm.mutex.RLock()
	defer sm.mutex.RUnlock()
	return sm.spinners
}

// SetSpinnersCount defines the amount of spinners managed by the manager.
// It will create empty spinners or delete last ones to match requested size.
func (sm *spinnerManager) SetSpinnersCount(count int) {
	sm.mutex.Lock()
	defer sm.mutex.Unlock()
	if count > len(sm.spinners) {
		for i := len(sm.spinners); i < count; i++ {
			sm.spinners = append(sm.spinners, sm.createSpinner(""))
		}
	} else {
		sm.spinners = sm.spinners[:count]
	}
}

// Start the rendering loop in a goroutine.
// Creates a interrupt signal if cancel context was not provided.
func (sm *spinnerManager) Start() {
	if sm.context == nil {
		ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt)
		go func() {
			<-ctx.Done()
			sm.Stop()
			cancel()
			os.Exit(0)
		}()
	}
	sm.ticks = time.NewTicker(100 * time.Millisecond)
	go sm.render()
}

// Stop sends a signal to the render goroutine to stop rendering.
// We then stop the ticker and persist the final frame for each spinner.
// Finally the deferred tput command will ensure tat the cursor is no longer hidden.
func (sm *spinnerManager) Stop() {
	sm.done <- true
	sm.ticks.Stop()

	// Persist the final frame for each spinner.
	sm.mutex.Lock()
	defer sm.mutex.Unlock()
	for _, s := range sm.spinners {
		s.Print(sm.writer, spinnerFrames[sm.frame])
	}
	clearEOS(sm.writer)
	cnorm(sm.writer)
	wrap(sm.writer)
}

// Render spinners in a dedicated goroutine spawned in start
//
// Each tick signal calls renderFrame which in turn will print the current
// frame to the writer provided by the manager.
func (sm *spinnerManager) render() {
	// Prepare the screen.
	civis(sm.writer)
	noWrap(sm.writer)
	defer cnorm(sm.writer)
	defer wrap(sm.writer)
	for {
		select {
		case <-sm.done:
			return
		case <-sm.hasUpdate:
			sm.renderFrame(false)
		case <-sm.ticks.C:
			sm.renderFrame(true)
		}
	}
}

func (sm *spinnerManager) renderFrame(animate bool) {
	spinners := sm.GetSpinners()
	linesCount := len(spinners)
	termHeight := ttyHeight()
	startLine := 0
	if termHeight != 0 && termHeight <= linesCount {
		startLine = linesCount - termHeight + 1
	}

	for i := startLine; i < linesCount; i++ {
		spinners[i].Print(sm.writer, spinnerFrames[sm.frame])
	}
	clearEOS(sm.writer)
	if linesCount > startLine {
		cuu(sm.writer, linesCount-startLine)
	}

	if animate {
		sm.setNextFrame()
	}
}

func getWriter() io.Writer {
	if runtime.GOOS == "windows" {
		return colorable.NewColorableStdout()
	} else {
		return os.Stdout
	}
}

func (sm *spinnerManager) setNextFrame() {
	sm.frame += 1
	if sm.frame >= len(spinnerFrames) {
		sm.frame = 0
	}
}

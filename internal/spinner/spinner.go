package spinner

import (
	"fmt"
	"io"
	"strings"
	"sync"
)

// Spinner manages a single spinner
type Spinner struct {
	mutex     sync.Mutex
	message   string
	status    spinnerStatus
	hasUpdate chan bool
}

type spinnerStatus int

const (
	Complete spinnerStatus = iota
	Error
	Suspended
	Awaiting
	Progressing
)

// Spinner animation
var spinnerFrames = []string{
	"⠈⠁",
	"⠈⠑",
	"⠈⠱",
	"⠈⡱",
	"⢀⡱",
	"⢄⡱",
	"⢄⡱",
	"⢆⡱",
	"⢎⡱",
	"⢎⡰",
	"⢎⡠",
	"⢎⡀",
	"⢎⠁",
	"⠎⠁",
	"⠊⠁",
}

// GetMessage returns the current spinner message.
func (s *Spinner) GetMessage() string {
	s.mutex.Lock()
	defer s.mutex.Unlock()

	return s.message
}

// UpdateMessage updates the spinner message.
func (s *Spinner) UpdateMessage(message string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	s.message = strings.ReplaceAll(message, "\n", "")
	s.notifyHasUpdate()
}

// UpdateMessagef updates the spinner message with a formatted string.
func (s *Spinner) UpdateMessagef(format string, a ...interface{}) {
	s.UpdateMessage(fmt.Sprintf(format, a...))
}

// SetStatus defines the status of the spinner.
func (s *Spinner) SetStatus(status spinnerStatus) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	s.status = status
}

// Print prints the spinner at a given position.
func (s *Spinner) Print(w io.Writer, char string) {
	s.mutex.Lock()
	defer s.mutex.Unlock()
	switch s.status {
	case Complete:
		fmt.Fprint(w, " \x1b[32m✓\x1b[0m ")
	case Error:
		fmt.Fprint(w, " \x1b[31m✗\x1b[0m ")
	case Suspended:
		fmt.Fprint(w, " \x1b[33m⏸\x1b[0m ")
	case Awaiting:
		fmt.Fprint(w, " ⌛ ")
	case Progressing:
		fmt.Fprintf(w, "\x1b[34m%s \x1b[0m", char)
	}
	fmt.Fprint(w, s.message)
	clearEOL(w)
	fmt.Fprint(w, "\r\n")
}

func (s *Spinner) notifyHasUpdate() {
	select {
	case s.hasUpdate <- true:
	default:
	}
}

type SpinnerOptions struct {
	Message   string
	HasUpdate chan bool
}

// NewSpinner creates a new spinner instance.
func NewSpinner(options SpinnerOptions) *Spinner {
	return &Spinner{
		message:   options.Message,
		hasUpdate: options.HasUpdate,
	}
}

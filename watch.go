/*
Copyright 2023 The Sylva authors


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"fmt"
	"io"
	"log"
	"os"
	"os/signal"
	"strings"
	"time"

	metav1 "k8s.io/apimachinery/pkg/apis/meta/v1"
	"k8s.io/client-go/rest"
	"sigs.k8s.io/controller-runtime/pkg/client"

	helmv2 "github.com/fluxcd/helm-controller/api/v2beta2"
	kustomizev1 "github.com/fluxcd/kustomize-controller/api/v1"

	"github.com/spf13/cobra"

	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/informer"
	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/inventory"
	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/report"
	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/spinner"
)

var watchCmd = &cobra.Command{
	Use:   "watch",
	Short: "Watch status of Flux resources",
	Long:  `The watch command shows the progress of flux resources reconciliation`,
	Example: `# Watch until all resources become ready
sylvactl watch

# Watch until a specific resource becomes ready with a timeout of 10 minutes
# (and fasten convergence by triggering reconciliation of dependents when a resource becomes ready)

sylvactl watch --reconcile --timeout 10m Kustomization/default/sylva-units`,
	RunE: watchCmdRun,
}

const (
	ReadyMessageAnnotation = "sylvactl/readyMessage"
	UnitReconcileTimeout   = "sylvactl/unitTimeout"
)

var (
	statusCmdArgs struct {
		ignoreSuspended bool
		log             bool
		once            bool
		reconcile       bool
		save            string
		unitTimeout     string
		timeout         string
		exitCondition   []string
		skipInventory   bool
	}
	ticker      = time.NewTicker(time.Second)
	unitTimeout time.Duration
)

type nodeMap map[string]informer.TreeNode

func init() {
	watchCmd.Flags().BoolVar(&statusCmdArgs.log, "log", false, "log resources changes rather than updating output (always true if output is not a terminal)")
	watchCmd.Flags().BoolVar(&statusCmdArgs.once, "once", false, "show current resource status and exit")
	watchCmd.Flags().BoolVar(&statusCmdArgs.reconcile, "reconcile", false, "fasten convergence by triggering reconciliation of dependents as soon as a flux resource becomes ready")
	watchCmd.Flags().BoolVar(&statusCmdArgs.ignoreSuspended, "ignore-suspended", false, "neither show nor wait for suspended resources")
	watchCmd.Flags().BoolVar(&statusCmdArgs.skipInventory, "skip-inventory", false, "Skip the display of inventory of stalled resources while exiting on errors")
	watchCmd.Flags().StringVar(&statusCmdArgs.unitTimeout, "unit-timeout", "0", "time to wait for flux unit to become ready (it activates the reconcile option). Non-zero values should contain a corresponding time unit (e.g., 1s, 2m, 3h). A value of zero (the default) means no timeout.")
	watchCmd.Flags().StringVar(&statusCmdArgs.timeout, "timeout", "0", "time to wait for flux resources to become ready. Non-zero values should contain a corresponding time unit (e.g., 1s, 2m, 3h). A value of zero (the default) means no timeout.")
	watchCmd.Flags().StringVar(&statusCmdArgs.save, "save", "", "save a report of watched flux resources in this file, as HTML")
	watchCmd.Flags().StringArrayVar(&statusCmdArgs.exitCondition, "exit-condition", []string{}, "Stop waiting and exit if a resource matches any of the status conditions")

	rootCmd.AddCommand(watchCmd)
	log.SetFlags(log.Ldate | log.Lmicroseconds)
}

func watchCmdRun(cmd *cobra.Command, args []string) error {
	exitStatus := 0
	var watchedResource string
	if len(args) >= 1 {
		watchedResource = args[0]
	} else {
		watchedResource = ""
	}

	timeout, err := time.ParseDuration(statusCmdArgs.timeout)
	if err != nil {
		return err
	}

	unitTimeout, err = time.ParseDuration(statusCmdArgs.unitTimeout)
	if err != nil {
		return err
	}
	if unitTimeout != 0 {
		statusCmdArgs.reconcile = true
	}

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	if timeout != 0 {
		ctx, cancel = context.WithTimeout(ctx, timeout)
	}
	defer cancel()

	cfg, err := kubeconfigArgs.ToRESTConfig()
	if err != nil {
		return err
	}

	exitConditions, err := parseExitCondition(statusCmdArgs.exitCondition)
	if err != nil {
		fmt.Println("Error parsing exit conditions:", err)
		os.Exit(1)
	}

	objUpdate := make(chan informer.ObjEvent, 1000)
	fluxInformer, err := informer.NewInformer(ctx, cfg, objUpdate, statusCmdArgs.reconcile, *kubeconfigArgs.Namespace)
	if err != nil {
		return err
	}

	if statusCmdArgs.log || !spinner.IsTerminal() {
		exitStatus = logResources(ctx, fluxInformer.ObjectMap, objUpdate, watchedResource, exitConditions)
	} else {
		log.SetOutput(io.Discard)
		sm := spinner.NewSpinnerManager(ctx)
		sm.Start()
		exitStatus = updateSpinners(ctx, fluxInformer.ObjectMap, sm, objUpdate, watchedResource, exitConditions)
		sm.Stop()
	}

	if exitStatus == 2 || exitStatus == 3 {
		progressUpdate, _ := computeProgressUpdate(fluxInformer.ObjectMap, watchedResource)
		timeoutSummary(exitStatus, cfg, progressUpdate, statusCmdArgs.skipInventory)
	}
	if statusCmdArgs.save != "" {
		if err := saveObjects(fluxInformer.ObjectMap.FilterDependencies(watchedResource), statusCmdArgs.save); err != nil {
			log.Fatal(err)
		}
	}

	defer os.Exit(exitStatus)
	return nil
}

func parseExitCondition(userConditions []string) ([]map[string]string, error) {
	var parsedConditions []map[string]string

	for _, condition := range userConditions {
		parsedCondition := strings.SplitN(condition, "=", 2)
		if len(parsedCondition) != 2 {
			return nil, fmt.Errorf("exit condition must be in 'key=value' format: %s", condition)
		}

		key := parsedCondition[0]
		value := parsedCondition[1]

		// Check if the key is either 'reason' or 'message'
		if key != "reason" && key != "message" {
			return nil, fmt.Errorf("exit condition must start with 'reason=' or 'message=': %s", condition)
		}

		// Remove potential surrounding quotes from value
		value = strings.Trim(value, "\"")
		parsedConditions = append(parsedConditions, map[string]string{key: value})
	}

	// If no user conditions are specified, append default conditions
	if len(parsedConditions) == 0 {
		parsedConditions = append(parsedConditions,
			map[string]string{"message": "values don't meet the specifications of the schema"},
			map[string]string{"message": "install retries exhausted"},
			map[string]string{"message": ": execution error at "},
		)
	}
	return parsedConditions, nil
}

func checkConditions(obj informer.TreeNode, exitConditions []map[string]string) error {
	if !informer.Reconcilable(obj) || !obj.HasHandledReconcileAnnotation() {
		return nil
	}
	for _, condition := range obj.GetConditions() {
		if condition.ObservedGeneration == obj.GetGeneration() {
			for _, exitCond := range exitConditions {
				for key, expectedValue := range exitCond {
					actualValue := ""
					if key == "reason" {
						actualValue = condition.Reason
					} else if key == "message" {
						actualValue = condition.Message
					}
					// Check for exact match for keys other than "message"
					if key != "message" && actualValue == expectedValue {
						return fmt.Errorf("Exit condition matched for unit %s with %s: %s\n", obj.DisplayName(), key, expectedValue)
					}
					// For "message" key, check if actualValue contains expectedValue
					if key == "message" && strings.Contains(actualValue, expectedValue) {
						return fmt.Errorf("Exit condition matched for unit %s with %s: %s\n", obj.DisplayName(), key, expectedValue)
					}
				}
			}
		}
	}
	return nil
}

func computeProgressUpdate(objMap informer.ObjectMap, watchedResource string) (nodeMap, bool) {
	allComplete := true
	progressUpdate := nodeMap{}
	for _, obj := range objMap.FilterDependencies(watchedResource).Items() {
		if obj.ReadyWithDependencies() || statusCmdArgs.ignoreSuspended && obj.Suspended() {
			continue
		}
		allComplete = false
		if showResource(obj, objMap) {
			progressUpdate[obj.QualifiedName()] = obj
		}
	}
	return progressUpdate, allComplete
}

func logSummary(progressUpdate nodeMap) {
	maxNameLength := 0
	for _, res := range progressUpdate {
		maxNameLength = max(maxNameLength, len(res.QualifiedName()))
	}
	formatString := fmt.Sprintf(" │  %%-%ds  %%s", maxNameLength)
	for _, res := range progressUpdate {
		log.Printf(formatString, res.DisplayName(), res.Status())
	}
}

func logResources(ctx context.Context, objMap informer.ObjectMap, updates chan informer.ObjEvent, watchedResource string, exitConditions []map[string]string) int {
	sortedMap := objMap.FilterDependencies(watchedResource).SortDependencies()
	progressing := nodeMap{}
	allComplete := true
	for _, obj := range sortedMap.Items() {
		if err := checkConditions(obj, exitConditions); err != nil {
			log.Println(err.Error())
			return 3
		}
		if !(obj.ReadyWithDependencies() || statusCmdArgs.ignoreSuspended && obj.Suspended()) {
			allComplete = false
		}
		if !showResource(obj, objMap) {
			continue
		} else if obj.QualifiedName() == watchedResource && obj.ReadyWithDependencies() {
			log.Printf("All Done: resource %s is ready!", watchedResource)
			return 0
		}

		log.Printf("%-50s %s", obj.DisplayName(), obj.Status())
	}
	if allComplete {
		if watchedResource == "" {
			log.Println("All Done: all resources are ready!")
			return 0
		} else {
			log.Printf("Flux object %s not found in this cluster", watchedResource)
			return 1
		}
	}
	for {
		select {
		case <-ctx.Done():
			switch ctx.Err() {
			case context.DeadlineExceeded:
				log.Println("Command timeout exceeded")
				return 2
			case context.Canceled:
				log.Println("Command was interrupted")
				return 1
			default:
				return 1
			}
		case <-ticker.C:
			if unitTimeout > 0 {
				if err := checkUnitsTimeouts(objMap); err != nil {
					log.Printf("Unit timeout exceeded: %s", err)
					return 1
				}
			}
		case event := <-updates:
			objName := event.FluxObject.QualifiedName()
			if event.Delete {
				log.Printf("%s has been deleted", objName)
				continue
			}
			// Retrieve node handling flux object
			obj, found := objMap.Get(objName)
			if !found {
				log.Printf("Warning: no treeNode found for object %s (status: %s)", objName, event.FluxObject.Status())
				continue
			}
			if watchedResource == "" {
				log.Printf("%s state changed: %s", obj.DisplayName(), obj.Status())
			} else {
				// When watchedResource is used, only display events related to the resource and its dependencies
				if obj.QualifiedName() == watchedResource {
					if obj.ReadyWithDependencies() {
						log.Printf("All Done: resource %s is ready!", watchedResource)
						return 0
					} else {
						log.Printf("%s state changed: %s", obj.DisplayName(), obj.Status())
					}
				} else if watched, found := objMap.Get(watchedResource); found {
					if depends, _ := watched.DependsOn(obj, true); depends {
						log.Printf("%s state changed: %s", obj.DisplayName(), obj.Status())
					} else {
						continue
					}
					// Object status change may have impacted the status of watchedResource
					if watched.ReadyWithDependencies() {
						log.Printf("All Done: resource %s is ready!", watchedResource)
						return 0
					}
				}
			}
			if err := checkConditions(obj, exitConditions); err != nil {
				log.Println(err.Error())
				return 3
			}
			progressUpdate, allComplete := computeProgressUpdate(objMap, watchedResource)
			if allComplete && watchedResource == "" {
				return 0
			} else if len(progressUpdate) > 0 && !sameObjectNames(progressing, progressUpdate) {
				log.Println(" ╭╴Waiting for the following resources to progress:")
				logSummary(progressUpdate)
				progressing = progressUpdate
				log.Println(" ╰╴╴╴┄")
			}
			if statusCmdArgs.once {
				return 0
			}
		}
	}
}

func updateSpinners(ctx context.Context, objMap informer.ObjectMap, sm spinner.SpinnerManager,
	updates chan informer.ObjEvent, watchedResource string, exitConditions []map[string]string) int {
	for {
		sortedMap := objMap.FilterDependencies(watchedResource).SortDependencies()
		index := 0
		allComplete := true
		// First list resource that are Ready
		for _, obj := range sortedMap.Items() {
			if err := checkConditions(obj, exitConditions); err != nil {
				sm.AddSpinner(fmt.Sprint(err.Error())).SetStatus(spinner.Error)
				return 3
			}
			if !(obj.ReadyWithDependencies() || statusCmdArgs.ignoreSuspended && obj.Suspended()) {
				allComplete = false
				continue
			} else if obj.QualifiedName() == watchedResource && obj.ReadyWithDependencies() {
				updateSpinner(obj, sm, index)
				sm.AddSpinner(fmt.Sprintf("All Done: resource %s is ready!", watchedResource)).SetStatus(spinner.Complete)
				return 0
			}
			if !(statusCmdArgs.ignoreSuspended && obj.Suspended()) {
				updateSpinner(obj, sm, index)
				index = index + 1
			}
		}
		// Then list resources that are progressing
		for _, obj := range sortedMap.Items() {
			if !showResource(obj, objMap) || obj.Ready() {
				continue
			}
			updateSpinner(obj, sm, index)
			index = index + 1
		}
		// Clear extra spinners if any
		sm.SetSpinnersCount(index)
		if allComplete {
			if watchedResource == "" {
				sm.AddSpinner("All Done!").SetStatus(spinner.Complete)
				return 0
			} else {
				sm.AddSpinner(fmt.Sprintf("Flux object %s not found in this cluster", watchedResource)).SetStatus(spinner.Error)
				return 1
			}
		}
		if statusCmdArgs.once {
			return 0
		}
		select {
		case <-ctx.Done():
			switch ctx.Err() {
			case context.DeadlineExceeded:
				sm.AddSpinner("Command timeout exceeded").SetStatus(spinner.Error)
				return 2
			case context.Canceled:
				sm.AddSpinner("Command was interrupted").SetStatus(spinner.Error)
				return 1
			default:
				return 1
			}
		case <-updates:
			continue
		case <-ticker.C:
			if unitTimeout > 0 {
				if err := checkUnitsTimeouts(objMap); err != nil {
					sm.AddSpinner(fmt.Sprintf("Unit timeout exceeded: %s", err)).SetStatus(spinner.Error)
					return 1
				}
			}
		}
	}
}

func updateSpinner(object informer.TreeNode, sm spinner.SpinnerManager, index int) {
	var sp *spinner.Spinner
	if index < len(sm.GetSpinners()) {
		sp = sm.GetSpinners()[index]
	} else {
		sp = sm.AddSpinner("")
	}
	if object.Ready() {
		if msg, ok := object.GetObject().GetAnnotations()[ReadyMessageAnnotation]; ok {
			sp.UpdateMessage(fmt.Sprintf("%s - Resource is ready: %s", object.DisplayName(), msg))
		} else {
			sp.UpdateMessage(fmt.Sprintf("%s - Resource is ready", object.DisplayName()))
		}
		sp.SetStatus(spinner.Complete)
	} else {
		sp.UpdateMessage(fmt.Sprintf("%s - %s", object.DisplayName(), strings.Replace(object.Status(), "\n", "", -1)))
		if object.Stalled() {
			sp.SetStatus(spinner.Error)
		} else if object.Suspended() {
			sp.SetStatus(spinner.Suspended)
		} else {
			sp.SetStatus(spinner.Progressing)
		}
	}
}

func showResource(obj informer.TreeNode, objMap informer.ObjectMap) bool {
	if statusCmdArgs.ignoreSuspended && obj.Suspended() {
		return false
	}
	for _, dep := range obj.GetDependencies() {
		if !dep.ReadyWithDependencies() {
			return false // don't show objects if their dependencies are not ready
		}
	}
	return true
}

func checkUnitsTimeouts(objMap informer.ObjectMap) error {
	for _, node := range objMap.Items() {
		if node.Ready() || !informer.Reconcilable(node) {
			continue
		}
		nodeTimeout := unitTimeout
		if customTimeout, err := time.ParseDuration(node.GetAnnotationValue(UnitReconcileTimeout)); err == nil {
			nodeTimeout = customTimeout
		}
		if node.CurrentReconcileDuration() > nodeTimeout {
			return fmt.Errorf("unit %s did not became ready after %s", node.DisplayName(), nodeTimeout)
		}
	}
	return nil
}

func saveObjects(objMap informer.ObjectMap, fileName string) error {

	units := []interface{}{}
	for _, node := range objMap.Items() {
		objCopy := node.ClientObject().DeepCopyObject().(client.Object)
		objCopy.SetManagedFields([]metav1.ManagedFieldsEntry{})
		units = append(units, objCopy)
	}
	return report.Generate(units, fileName)

}

func timeoutSummary(exitStatus int, clientConfig *rest.Config, stalledMap nodeMap, skipInventory bool) {
	if exitStatus == 2 {
		fmt.Println("Timed-out waiting for the following resources to be ready:")
	} else if exitStatus == 3 {
		fmt.Println("Following resources were not ready when exit condition matched:")
	}
	if skipInventory {
		for _, res := range stalledMap {
			fmt.Printf("    * %s: %s\n", res.DisplayName(), res.Status())
		}
	} else {
		// Build list of stalled objects
		stalledObjs := []client.Object{}
	OUTER:
		for _, res := range stalledMap {
			// Exclude HelmReleases produced by a Kustomization to avoid duplicates
			if res.GetObjectKind().GroupVersionKind().Kind == helmv2.HelmReleaseKind {
				for _, other := range stalledMap {
					if other.GetObjectKind().GroupVersionKind().Kind == kustomizev1.KustomizationKind &&
						other.GetNamespace() == res.GetNamespace() &&
						other.GetName() == res.GetName() {
						continue OUTER
					}
				}
			}
			stalledObjs = append(stalledObjs, res.ClientObject())
		}
		inventory.PrintResourcesStatus(context.Background(), inventory.InventoryOptions{SkipReadyResources: true}, clientConfig, stalledObjs...)
	}
}

// Check if the two maps contain the same objects names
func sameObjectNames(m nodeMap, n nodeMap) bool {
	if len(m) != len(n) {
		return false
	}
	for k := range m {
		if _, ok := n[k]; !ok {
			return false
		}
	}
	return true
}

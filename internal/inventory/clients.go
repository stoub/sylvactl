package inventory

import (
	"context"
	"fmt"

	corev1 "k8s.io/api/core/v1"
	"k8s.io/apimachinery/pkg/runtime"
	"k8s.io/apimachinery/pkg/types"
	"k8s.io/client-go/rest"
	"k8s.io/client-go/tools/clientcmd"
	"sigs.k8s.io/controller-runtime/pkg/client"

	helmv2 "github.com/fluxcd/helm-controller/api/v2beta2"
	kustomizev1 "github.com/fluxcd/kustomize-controller/api/v1"
	"github.com/fluxcd/pkg/apis/meta"
	sourcev1 "github.com/fluxcd/source-controller/api/v1"
	sourcev1beta2 "github.com/fluxcd/source-controller/api/v1beta2"
)

func getRemoteConfig(ctx context.Context, kubeConfigRef *meta.KubeConfigReference, kubeConfigNamespace string, kubeClient client.Client) (*rest.Config, error) {

	secretName := types.NamespacedName{
		Namespace: kubeConfigNamespace,
		Name:      kubeConfigRef.SecretRef.Name,
	}

	var secret corev1.Secret
	if err := kubeClient.Get(ctx, secretName, &secret); err != nil {
		return nil, fmt.Errorf("unable to read KubeConfig secret '%s' error: %w", secretName.String(), err)
	}

	var kubeConfig []byte
	switch {
	case kubeConfigRef.SecretRef.Key != "":
		key := kubeConfigRef.SecretRef.Key
		kubeConfig = secret.Data[key]
		if kubeConfig == nil {
			return nil, fmt.Errorf("KubeConfig secret '%s' does not contain a '%s' key with a kubeconfig", secretName, key)
		}
	case secret.Data["value"] != nil:
		kubeConfig = secret.Data["value"]
	case secret.Data["value.yaml"] != nil:
		kubeConfig = secret.Data["value.yaml"]
	default:
		// User did not specify a key, and the 'value' key was not defined.
		return nil, fmt.Errorf("KubeConfig secret '%s' does not contain a 'value' key with a kubeconfig", secretName)
	}

	restConfig, err := clientcmd.RESTConfigFromKubeConfig(kubeConfig)
	if err != nil {
		return nil, err
	}

	return restConfig, nil
}

func GetClient(clientConfig *rest.Config) (client.Client, error) {
	// Disable client rate limits to avoid warnings
	clientConfig.Burst = -1
	clientConfig.QPS = -1
	clientConfig.WarningHandler = NoopWarningLogger{}

	scheme := runtime.NewScheme()
	_ = corev1.AddToScheme(scheme)
	_ = kustomizev1.AddToScheme(scheme)
	_ = helmv2.AddToScheme(scheme)
	_ = sourcev1.AddToScheme(scheme)
	_ = sourcev1beta2.AddToScheme(scheme)

	return client.New(clientConfig, client.Options{Scheme: scheme})
}

type NoopWarningLogger struct{}

func (NoopWarningLogger) HandleWarningHeader(code int, agent string, message string) {
}

/*
Copyright 2022 The Sylva authors

Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.

*/

package informer

import (
	"errors"
	"fmt"
	"log"
	"reflect"
	"sort"
	"sync"
)

const (
	ready readyWithDependencies = iota
	notReady
	unknown
)

type (
	objectMap struct {
		ObjectMap  map[string]TreeNode
		ObjectTree []TreeNode
		lock       sync.RWMutex
	}

	ObjectMap interface {
		Store(object FluxObject, updateDeps bool) (FluxObject, TreeNode, error)
		Remove(object FluxObject)
		Items() []TreeNode
		add(node TreeNode)
		Get(qualifiedName string) (TreeNode, bool)
		get(qualifiedName string) (TreeNode, bool)
		ProcessReadyWithDeps()
		FilterDependencies(qualifiedName string) ObjectMap
		SortDependencies() ObjectMap
		// Sort interface
		Len() int
		Swap(i, j int)
		Less(i, j int) bool
	}

	treeNode struct {
		FluxObject                       // Actually we only need QualifiedName() and ListDependencies() from that interface
		Dependencies          []TreeNode `json:"objectTree,omitempty"`
		readyWithDependencies readyWithDependencies
		lock                  sync.RWMutex
	}

	readyWithDependencies int

	TreeNode interface {
		FluxObject
		GetObject() FluxObject
		SetObject(object FluxObject)
		DependsOn(object FluxObject, indirectly bool) (bool, string)
		GetDependencies() []TreeNode
		UpdateDependencies(tree ObjectMap) error
		ReadyWithDependencies() bool
		setReadyWithDeps(ready readyWithDependencies)
		getReadyWithDeps() readyWithDependencies
		computeReadyWithDeps()
	}
)

// objectMap
func NewMap() ObjectMap {
	return &objectMap{
		ObjectMap:  map[string]TreeNode{},
		ObjectTree: []TreeNode{},
	}
}

// Stores object in map, update dependencies, return node and previous Object if there was any
func (m *objectMap) Store(obj FluxObject, updateDeps bool) (FluxObject, TreeNode, error) {
	m.lock.Lock()
	defer m.lock.Unlock()
	var oldObject FluxObject
	var err error
	node, found := m.get(obj.QualifiedName())
	if !found {
		node = New(obj)
		m.ObjectTree = append(m.ObjectTree, node)
		m.ObjectMap[node.QualifiedName()] = node
		// As we have a new object, reprocess dependencies of all objects that have missing ones
		for i := range m.ObjectTree {
			if m.ObjectTree[i].HasMissingDependencies() && updateDeps {
				err = errors.Join(err, m.ObjectTree[i].UpdateDependencies(m))
			}
		}
	} else {
		oldObject = node.GetObject()
		node.SetObject(obj)
	}
	if oldObject == nil || !reflect.DeepEqual(oldObject.ListDependencies(), node.ListDependencies()) {
		err = errors.Join(err, node.UpdateDependencies(m))
	}
	return oldObject, node, err
}

// Add a node to tree without any check, the caller must ensure that the resulting
// tree will be consistent, as node duplication or missingDependencies won't be calculated
func (m *objectMap) add(node TreeNode) {
	m.ObjectTree = append(m.ObjectTree, node)
	m.ObjectMap[node.QualifiedName()] = node
}

func (m *objectMap) Remove(obj FluxObject) {
	m.lock.Lock()
	defer m.lock.Unlock()
	qualifiedName := obj.QualifiedName()
	if removedObject, found := m.get(qualifiedName); found {
		j := -1
		// Remove object from other's dependencies
		for i := range m.ObjectTree {
			if depends, _ := m.ObjectTree[i].DependsOn(removedObject, false); depends {
				//nolint:errcheck
				defer m.ObjectTree[i].UpdateDependencies(m)
			}
			if m.ObjectTree[i].QualifiedName() == qualifiedName {
				j = i
			}
		}
		// Remove object from ObjectMap
		if j != -1 {
			if len(m.ObjectTree) > 1 {
				m.ObjectTree[j] = m.ObjectTree[len(m.ObjectTree)-1]
				m.ObjectTree = m.ObjectTree[:len(m.ObjectTree)-1]
			} else {
				m.ObjectTree = []TreeNode{}
			}
		} // else log.Printf("Error: object %s was in ObjectMap but not in ObjectTree", qualifiedName)
		delete(m.ObjectMap, qualifiedName)
	}
}

func (m *objectMap) get(qualifiedName string) (TreeNode, bool) {
	t, found := m.ObjectMap[qualifiedName]
	return t, found
}

func (m *objectMap) Get(qualifiedName string) (TreeNode, bool) {
	m.lock.RLock()
	defer m.lock.RUnlock()
	t, found := m.ObjectMap[qualifiedName]
	return t, found
}

func (m *objectMap) Items() []TreeNode {
	m.lock.RLock()
	defer m.lock.RUnlock()
	return m.ObjectTree
}

// Process all objects to check if they are Ready as well as all their dependencies
func (m *objectMap) ProcessReadyWithDeps() {
	m.lock.RLock()
	defer m.lock.RUnlock()
	for _, obj := range m.ObjectTree {
		obj.setReadyWithDeps(unknown)
	}
	allProcessed := false
	for !allProcessed {
		allProcessed = true
		for _, obj := range m.ObjectTree {
			if obj.getReadyWithDeps() != unknown {
				continue
			}
			obj.computeReadyWithDeps()
			if obj.getReadyWithDeps() == unknown {
				allProcessed = false
			}
		}
	}
}

// Sort the list of objects by their order of dependency in a deterministic way
func (m *objectMap) SortDependencies() ObjectMap {
	m.lock.Lock()
	defer m.lock.Unlock()
	// Sort alphabetically first to have a deterministic ordering
	sort.Sort(m)
	ordered := NewMap()
	missing := map[string]TreeNode{}
	for {
	OUTER:
		for _, obj := range m.ObjectTree {
			if _, ok := ordered.get(obj.QualifiedName()); ok {
				continue // object is already in sorted map
			}
			for _, dep := range obj.GetDependencies() {
				if _, ok := ordered.get(dep.QualifiedName()); !ok {
					if _, ok := m.get(dep.QualifiedName()); ok {
						// wait for object"s dependency to be stored in map first
						continue OUTER
					} else {
						// object dependency is not in sorted Map
						missing[dep.QualifiedName()] = dep
					}
				}
			}
			ordered.add(obj)
		}
		if ordered.Len()+len(missing) == m.Len() {
			break
		}
	}
	return ordered
}

// Filter objectMap by selecting the dependencies of provided rootNode if any
// if rootNode is empty, return the original Map
// if rootNode is not found in the original Map, returns an empty map
func (m *objectMap) FilterDependencies(rootNode string) ObjectMap {
	if rootNode == "" {
		return m
	}
	m.lock.RLock()
	defer m.lock.RUnlock()
	filtered := NewMap()
	obj, ok := m.get(rootNode)
	if !ok {
		return filtered
	}
	filtered.add(obj)
	getAllDependencies(obj, filtered)
	return filtered
}

func getAllDependencies(obj TreeNode, m ObjectMap) {
	for _, dep := range obj.GetDependencies() {
		if _, found := m.get(dep.QualifiedName()); !found {
			m.add(dep)
			getAllDependencies(dep, m)
		}
	}
}

// sort.Sort interface implementation for objectMap
func (m *objectMap) Len() int {
	return len(m.ObjectTree)
}

func (m *objectMap) Swap(i, j int) {
	m.ObjectTree[i], m.ObjectTree[j] = m.ObjectTree[j], m.ObjectTree[i]
}

func (m *objectMap) Less(i, j int) bool {
	return m.ObjectTree[i].QualifiedName() < m.ObjectTree[j].QualifiedName()
}

// treeObject

func New(object FluxObject) *treeNode {
	return &treeNode{
		FluxObject:            object,
		Dependencies:          []TreeNode{},
		readyWithDependencies: unknown,
	}
}

func (t *treeNode) GetObject() FluxObject {
	return t.FluxObject
}

func (t *treeNode) SetObject(obj FluxObject) {
	t.FluxObject = obj
}

func (t *treeNode) getReadyWithDeps() readyWithDependencies {
	return t.readyWithDependencies
}

func (t *treeNode) setReadyWithDeps(ready readyWithDependencies) {
	t.readyWithDependencies = ready
}

func (t *treeNode) computeReadyWithDeps() {
	if !t.Ready() {
		t.setReadyWithDeps(notReady)
		return
	}
	for _, dep := range t.GetDependencies() {
		if dep.getReadyWithDeps() == unknown {
			t.setReadyWithDeps(unknown)
			return
		} else if dep.getReadyWithDeps() == notReady {
			t.setReadyWithDeps(notReady)
			return
		}
	}
	// all deps are ready as well as current resource
	t.setReadyWithDeps(ready)
}

func (t *treeNode) DependsOn(obj FluxObject, indirectly bool) (bool, string) {
	t.lock.RLock()
	defer t.lock.RUnlock()
	for _, child := range t.Dependencies {
		if child.QualifiedName() == obj.QualifiedName() {
			return true, fmt.Sprintf("%s depends on %s", t.DisplayName(), obj.DisplayName())
		} else if indirectly {
			if depends, msg := child.DependsOn(obj, true); depends {
				return depends, fmt.Sprintf("%s depends on %s, %s", t.DisplayName(), child.DisplayName(), msg)
			}
		}
	}
	return false, ""
}

func (t *treeNode) GetDependencies() []TreeNode {
	t.lock.RLock()
	defer t.lock.RUnlock()
	return t.Dependencies
}

func (t *treeNode) UpdateDependencies(tree ObjectMap) error {
	t.lock.Lock()
	defer t.lock.Unlock()
	t.Dependencies = []TreeNode{}
	missingDependencies := []string{}
	for _, dep := range t.ListDependencies() {
		depNode, found := tree.get(dep)
		if found {
			if cycle, msg := depNode.DependsOn(t, true); cycle {
				return fmt.Errorf("object %s can't depend on %s as it would create a cycle: %s", t.QualifiedName(), depNode.QualifiedName(), msg)
			}
			t.Dependencies = append(t.Dependencies, depNode)
		} else {
			missingDependencies = append(missingDependencies, dep)
		}
	}
	t.SetMissingDependencies(missingDependencies)
	return nil
}

// Check if object is Ready as well as all its direct and indirect dependencies
func (t *treeNode) ReadyWithDependencies() bool {
	switch t.getReadyWithDeps() {
	case ready:
		return true
	case notReady:
		return false
	default:
		log.Printf("Warning: readyWithDependencies was not processed for %s", t.DisplayName())
		return false
	}
}

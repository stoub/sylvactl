/*
Copyright 2023 The Sylva authors


Licensed under the Apache License, Version 2.0 (the "License");
you may not use this file except in compliance with the License.
You may obtain a copy of the License at

    http://www.apache.org/licenses/LICENSE-2.0

Unless required by applicable law or agreed to in writing, software
distributed under the License is distributed on an "AS IS" BASIS,
WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
See the License for the specific language governing permissions and
limitations under the License.
*/

package main

import (
	"context"
	"fmt"
	"log"
	"os"
	"os/signal"
	"strings"

	"k8s.io/apimachinery/pkg/types"

	"github.com/spf13/cobra"

	helmv2 "github.com/fluxcd/helm-controller/api/v2beta2"
	kustomizev1 "github.com/fluxcd/kustomize-controller/api/v1"

	"gitlab.com/sylva-projects/sylva-elements/sylvactl/internal/inventory"
)

var inventoryCmd = &cobra.Command{
	Use:   "inventory",
	Short: "Print inventory of Flux resources",
	Long:  `The inventory command shows the detailed inventory of flux Kustomizations and HelmReleases`,
	Example: `# Show inventory of sylva-units Kustomization in sylva-system namespace
sylvactl inventory Kustomization/sylva-system/sylva-units`,
	RunE: inventoryCmdRun,
}

func init() {
	rootCmd.AddCommand(inventoryCmd)
	//log.SetFlags(0)
}

func inventoryCmdRun(cmd *cobra.Command, args []string) error {
	if len(args) != 1 {
		log.Fatal("You should provide a resource identifier as argument")
	}
	resourceIdentifier := strings.Split(args[0], "/")
	if len(resourceIdentifier) != 3 {
		log.Fatal("Resource identifier should be provided as Kind/namespace/name")
	}

	ctx, cancel := signal.NotifyContext(context.Background(), os.Interrupt, os.Kill)
	defer cancel()

	cfg, err := kubeconfigArgs.ToRESTConfig()
	if err != nil {
		return err
	}

	kubeClient, err := inventory.GetClient(cfg)
	if err != nil {
		return err
	}

	resourceName := types.NamespacedName{
		Namespace: resourceIdentifier[1],
		Name:      resourceIdentifier[2],
	}

	if resourceIdentifier[0] == "Kustomization" {
		ks := &kustomizev1.Kustomization{}
		if err := kubeClient.Get(ctx, resourceName, ks); err != nil {
			return fmt.Errorf("failed to get Kustomization %s: %w", resourceName, err)
		}
		inventory.PrintResourcesStatus(ctx, inventory.InventoryOptions{}, cfg, ks)

	} else if resourceIdentifier[0] == "HelmRelease" {
		hr := &helmv2.HelmRelease{}
		if err := kubeClient.Get(ctx, resourceName, hr); err != nil {
			return fmt.Errorf("failed to get HelmRelease %s: %w", resourceName, err)
		}
		inventory.PrintResourcesStatus(ctx, inventory.InventoryOptions{}, cfg, hr)
	} else {
		return fmt.Errorf("\"%s\" is not a valid Kind for inventory command, it should be a Kustomization or HelmRelease", resourceIdentifier[0])
	}
	return nil
}
